const http = require("http");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");
const htmlPath = "../data/code.html";
const stringPath = "../data/string.json";
const statusCodes = require("http").STATUS_CODES;

http
  .createServer((request, response) => {
    if (request.method === "GET" && request.url === "/html") {

      const htmlFile = fs.readFileSync(htmlPath);
      response.writeHead(200);
      response.end(htmlFile);

    } else if (request.method === "GET" && request.url === "/json") {

      const JSONFile = fs.readFileSync(stringPath);
      response.writeHead(200);
      response.end(JSONFile.toString());

    } else if (request.method === "GET" && request.url === "/uuid") {
      response.writeHead(200);
      const res = {
        uuid: uuidv4(),
      };

      response.end(JSON.stringify(res));

    } else if (request.method === "GET" && request.url.match("/status/?")) {
      const statusCode = Number(request.url.split("/")[2]);

      if (statusCodes[statusCode]) {

        response.writeHead(200);
        response.end(statusCodes[statusCode]);
      } 
      else {
        response.writeHead(404);
        response.end("Statuscode not found !");
      }
    } 
    else if (request.method === "GET" && request.url.match("/delay/?")) {
      const delay = Number(request.url.split("/")[2]);

      if (delay !== NaN) {
        setTimeout(() => {
          response.writeHead(200);
          response.end(statusCodes[200]);
        }, delay * 1000);
      } 
      else {
        response.writeHead(404);
        response.end("Error !");
      }
    } 
    else {
      response.end("Invalid url");
    }
  })
  .listen(8080);
